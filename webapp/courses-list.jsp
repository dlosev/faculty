<%@ page import="com.ldv.faculty.model.Course" %>
<%@ page import="java.util.Collection" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Courses</title>

    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet"/>
</head>
<body>
<a href="${pageContext.request.contextPath}/logout">
    <button class="logout-btn">Logout</button>
</a>
<h2>Courses</h2>
<a href="${pageContext.request.contextPath}/course/new"><button>Create Course</button></a>
<% Collection<Course> courses = (Collection<Course>) request.getAttribute("courses"); %>
<% if (courses.isEmpty()) { %>
    <p>No courses</p>
<% } else { %>
    <ul>
        <% for (Course c : courses) { %>
        <li>
            <a href="${pageContext.request.contextPath}/course?id=<%= c.getId() %>"><%= c.getName() %></a>
        </li>
        <% } %>
    </ul>
<% } %>
</body>
</html>