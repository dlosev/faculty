<%@ page import="com.ldv.faculty.model.Instructor" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<% if (request.getSession().getAttribute("user") instanceof Instructor) { %>
    <% response.sendRedirect(request.getContextPath() + "/course"); %>
<% } else { %>
    <%@ include file="student.jsp" %>
<% } %>
