<%@ page import="com.ldv.faculty.model.Comment" %>
<%@ page import="com.ldv.faculty.model.Post" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Discussion Board</title>

    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet"/>
</head>
<body>
<h2>Discussion Board</h2>
<% if (request.getAttribute("errors") != null) { %>
<ul class="error-block">
    <% for (String error : (Collection<String>) request.getAttribute("errors")) { %>
    <li><%= error %></li>
    <% } %>
</ul>
<% } %>
<% Post post = (Post) request.getAttribute("post"); %>
<% Comment comment = (Comment) request.getAttribute("comment"); %>
<% Collection<Post> posts = (Collection<Post>) request.getAttribute("posts"); %>
<form method="post">
    <label for="message">Message:</label>
    <br/>
    <textarea rows="8" id="message" name="message"><%= post.getText() == null ? "" : post.getText() %></textarea>
    <br/>
    <button type="submit">Post Message</button>
</form>
<% if (posts.isEmpty()) { %>
<p>No messages</p>
<% } else { %>
<ul>
    <li>
    <% for (Post p : posts) { %>
        <div class="post">
            <div class="post-user"><%= p.getUser().getFirstName() %> <%= p.getUser().getLastName() %></div>
            <div class="post-date"><%= p.getCreatedAt().format(DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm")) %></div>
            <div><%= p.getText() %></div>
        </div>
        <% if (p.getComments() != null && !p.getComments().isEmpty()) { %>
        <ul>
        <% for (Comment c : p.getComments()) { %>
            <li class="comment">
                <div class="post-user"><%= c.getUser().getFirstName() %> <%= c.getUser().getLastName() %></div>
                <div class="post-date"><%= c.getCreatedAt().format(DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm")) %></div>
                <div><%= c.getText() %></div>
            </li>
        <% } %>
        </ul>
        <% } %>
        <form action="?id=<%= request.getParameter("id") %>&postId=<%= p.getId() %>" method="post">
            <label for="comment">Comment:</label>
            <br/>
            <textarea rows="8" id="comment" name="comment"><%= comment.getText() == null ? "" : comment.getText() %></textarea>
            <br/>
            <button type="submit">Post Comment</button>
        </form>
    <% } %>
    </li>
</ul>
<% } %>
<a class="back-btn" href="${pageContext.request.contextPath}/course?id=<%= request.getParameter("id") %>">
    <button>Back</button>
</a>
</body>
</html>