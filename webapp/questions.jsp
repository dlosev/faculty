<%@ page import="com.ldv.faculty.model.CourseQuestion" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create Questions</title>

    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet"/>
</head>
<body>
<h2>Create Questions</h2>
<% if (request.getAttribute("errors") != null) { %>
<ul class="error-block">
    <% for (String error : (Collection<String>) request.getAttribute("errors")) { %>
    <li><%= error %></li>
    <% } %>
</ul>
<% } %>
<% List<CourseQuestion> questions = (List<CourseQuestion>) request.getAttribute("questions"); %>
<form class="questions" method="post">
    <label for="text1">Question 1:</label>
    <br/>
    <textarea rows="4" id="text1" name="text1"><%= questions.get(0).getText() == null ? "" : questions.get(0).getText() %></textarea>
    <br/>
    <label>Options:</label>
    <ol>
        <li>
            <input type="radio" name="correctAnswerNumber1" <%= questions.get(0).getCorrectAnswerNumber() <= 1 ? "checked" : "" %> value="1">
            <input type="text" name="answer11" value="<%= questions.get(0).getAnswer1() == null ? "" : questions.get(0).getAnswer1() %>"/>
        </li>
        <li>
            <input type="radio" name="correctAnswerNumber1" <%= questions.get(0).getCorrectAnswerNumber() == 2 ? "checked" : "" %> value="2">
            <input type="text" name="answer12" value="<%= questions.get(0).getAnswer2() == null ? "" : questions.get(0).getAnswer2() %>"/>
        </li>
        <li>
            <input type="radio" name="correctAnswerNumber1" <%= questions.get(0).getCorrectAnswerNumber() == 3 ? "checked" : "" %> value="3">
            <input type="text" name="answer13" value="<%= questions.get(0).getAnswer3() == null ? "" : questions.get(0).getAnswer3() %>"/>
        </li>
        <li>
            <input type="radio" name="correctAnswerNumber1" <%= questions.get(0).getCorrectAnswerNumber() == 4 ? "checked" : "" %> value="4">
            <input type="text" name="answer14" value="<%= questions.get(0).getAnswer4() == null ? "" : questions.get(0).getAnswer4() %>"/>
        </li>
    </ol>

    <label for="text2">Question 2:</label>
    <br/>
    <textarea rows="4" id="text2" name="text2"><%= questions.get(1).getText() == null ? "" : questions.get(1).getText() %></textarea>
    <br/>
    <label>Options:</label>
    <ol>
        <li>
            <input type="radio" name="correctAnswerNumber2" <%= questions.get(1).getCorrectAnswerNumber() <= 1 ? "checked" : "" %> value="1">
            <input type="text" name="answer21" value="<%= questions.get(1).getAnswer1() == null ? "" : questions.get(1).getAnswer1() %>"/>
        </li>
        <li>
            <input type="radio" name="correctAnswerNumber2" <%= questions.get(1).getCorrectAnswerNumber() == 2 ? "checked" : "" %> value="2">
            <input type="text" name="answer22" value="<%= questions.get(1).getAnswer2() == null ? "" : questions.get(1).getAnswer2() %>"/>
        </li>
        <li>
            <input type="radio" name="correctAnswerNumber2" <%= questions.get(1).getCorrectAnswerNumber() == 3 ? "checked" : "" %> value="3">
            <input type="text" name="answer23" value="<%= questions.get(1).getAnswer3() == null ? "" : questions.get(1).getAnswer3() %>"/>
        </li>
        <li>
            <input type="radio" name="correctAnswerNumber2" <%= questions.get(1).getCorrectAnswerNumber() == 4 ? "checked" : "" %> value="4">
            <input type="text" name="answer24" value="<%= questions.get(1).getAnswer4() == null ? "" : questions.get(1).getAnswer4() %>"/>
        </li>
    </ol>
    <button type="submit">Save</button>
</form>
<a class="back-btn" href="${pageContext.request.contextPath}/course/assignment?id=<%= request.getParameter("id") %>">
    <button>Back</button>
</a>
</body>
</html>