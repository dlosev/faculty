<%@ page import="com.ldv.faculty.model.CourseQuestion" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Assignment</title>

    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet"/>
</head>
<body>
<h2>Assignment</h2>
<% List<CourseQuestion> questions = (List<CourseQuestion>) request.getAttribute("questions"); %>
<% if (questions.isEmpty()) { %>
    <a href="questions?id=<%= request.getParameter("id") %>"><button>Create Questions</button></a>
<% } else { %>
<ol>
    <li>
        <%= questions.get(0).getText() %>
        <ol type="a">
            <li class="<%= questions.get(0).getCorrectAnswerNumber() == 1 ? "correct-answer" : "" %>"><%= questions.get(0).getAnswer1() %></li>
            <li class="<%= questions.get(0).getCorrectAnswerNumber() == 2 ? "correct-answer" : "" %>"><%= questions.get(0).getAnswer2() %></li>
            <li class="<%= questions.get(0).getCorrectAnswerNumber() == 3 ? "correct-answer" : "" %>"><%= questions.get(0).getAnswer3() %></li>
            <li class="<%= questions.get(0).getCorrectAnswerNumber() == 4 ? "correct-answer" : "" %>"><%= questions.get(0).getAnswer4() %></li>
        </ol>
    </li>
    <li>
        <%= questions.get(1).getText() %>
        <ol type="a">
            <li class="<%= questions.get(1).getCorrectAnswerNumber() == 1 ? "correct-answer" : "" %>"><%= questions.get(1).getAnswer1() %></li>
            <li class="<%= questions.get(1).getCorrectAnswerNumber() == 2 ? "correct-answer" : "" %>"><%= questions.get(1).getAnswer2() %></li>
            <li class="<%= questions.get(1).getCorrectAnswerNumber() == 3 ? "correct-answer" : "" %>"><%= questions.get(1).getAnswer3() %></li>
            <li class="<%= questions.get(1).getCorrectAnswerNumber() == 4 ? "correct-answer" : "" %>"><%= questions.get(1).getAnswer4() %></li>
        </ol>
    </li>
</ol>
<% } %>
<a class="back-btn" href="${pageContext.request.contextPath}/course?id=<%= request.getParameter("id") %>">
    <button>Back</button>
</a>
</body>
</html>