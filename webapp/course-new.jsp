<%@ page import="com.ldv.faculty.model.Course" %>
<%@ page import="java.util.Collection" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create Course</title>

    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet"/>
</head>
<body>
<h2>Create Course</h2>
<% if (request.getAttribute("errors") != null) { %>
<ul class="error-block">
    <% for (String error : (Collection<String>) request.getAttribute("errors")) { %>
        <li><%= error %></li>
    <% } %>
</ul>
<% } %>
<% Course course = (Course) request.getAttribute("course"); %>
<form method="post">
    <div class="control">
        <label for="name">Name:</label>
        <input type="text" id="name" name="name" value="<%= course.getName() == null ? "" : course.getName() %>" />
    </div>
    <div class="control">
        <label for="description">Info:</label>
        <textarea rows="12" id="description" name="description"><%= course.getDescription() == null ? "" : course.getDescription() %></textarea>
    </div>

    <button type="submit">Save</button>
</form>
<a class="back-btn" href="${pageContext.request.contextPath}/course">
    <button>Back</button>
</a>
</body>
</html>