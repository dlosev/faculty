<%@ page import="com.ldv.faculty.model.User" %>
<%@ page import="java.util.Collection" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>

    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css"/>
</head>
<body>

<% if (request.getAttribute("errors") != null) { %>
<ul class="error-block">
    <% for (String error : (Collection<String>) request.getAttribute("errors")) { %>
        <li><%= error %></li>
    <% } %>
</ul>
<% } %>
<% User user = (User) request.getAttribute("user"); %>
<form class="login-form" action="${pageContext.request.contextPath}/registration" method="post">
    <div class="control">
        <label for="firstName">First Name:</label>
        <input type="text" id="firstName" name="firstName" value="<%= user.getFirstName() == null ? "" : user.getFirstName() %>" />
    </div>
    <div class="control">
        <label for="lastName">Last Name:</label>
        <input type="text" id="lastName" name="lastName" value="<%= user.getLastName() == null ? "" : user.getLastName() %>" />
    </div>
    <div class="control">
        <label for="username">ID:</label>
        <input type="text" id="username" name="username" value="<%= user.getUsername() == null ? "" : user.getUsername() %>" />
    </div>
    <div class="control">
        <label for="password">Password:</label>
        <input type="password" id="password" name="password" />
    </div>
    <div class="control">
        <label for="passwordConfirm">Confirm Password:</label>
        <input type="password" id="passwordConfirm" name="passwordConfirm" />
    </div>
    <div class="control">
        <label><input type="radio" name="instructor" <%= Boolean.valueOf(request.getParameter("instructor")) ? "checked" : "" %> value="true">Instructor</label>
        <label><input type="radio" name="instructor" <%= !Boolean.valueOf(request.getParameter("instructor")) ? "checked" : "" %> value="false">Student</label>
    </div>

    <button type="submit">Register</button>
</form>
<a class="back-btn" href="${pageContext.request.contextPath}/">
    <button>Back</button>
</a>
</body>
</html>
