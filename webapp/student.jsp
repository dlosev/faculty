<%@ page import="com.ldv.faculty.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home Page</title>

    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet"/>
</head>
<body>
<a href="${pageContext.request.contextPath}/logout">
    <button class="logout-btn">Logout</button>
</a>

<h3>Hello, <%= ((User) request.getSession().getAttribute("user")).getFirstName() %>!</h3>
</body>
</html>
