<%@ page import="com.ldv.faculty.model.Course" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<% Course course = (Course) request.getAttribute("course"); %>
<head>
    <title><%= course.getName() %></title>

    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet"/>
</head>
<body>
<h2><%= course.getName() %></h2>
<div><a href="course/content?id=<%= course.getId() %>">Content</a></div>
<div><a href="course/assignment?id=<%= course.getId() %>">Assignment</a></div>
<div><a href="course/discussion-board?id=<%= course.getId() %>">Discussion Board</a></div>
<div><a href="course/information?id=<%= course.getId() %>">Information</a></div>
<div><a href="course/grade-center?id=<%= course.getId() %>">Grade Center</a></div>
<a class="back-btn" href="${pageContext.request.contextPath}/course">
    <button>Back</button>
</a>
</body>
</html>