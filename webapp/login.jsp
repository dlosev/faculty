<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>

    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css"/>
</head>
<body>

<% if (request.getAttribute("error") != null) { %>
<div class="error-block">
    <%= request.getAttribute("error") %>
</div>
<% } %>

<form class="login-form" action="${pageContext.request.contextPath}/login" method="post">
    <div class="control">
        <label for="username">ID:</label>
        <input type="text" id="username" name="username" />
    </div>

    <div class="control">
        <label for="password">Password:</label>
        <input type="password" id="password" name="password" />
    </div>

    <button type="submit">Log in</button>
    <a class="registration-btn" href="${pageContext.request.contextPath}/registration">
        <button type="button">Registration</button>
    </a>
</form>
</body>
</html>
