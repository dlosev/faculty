<%@ page import="com.ldv.faculty.model.Course" %>
<%@ page import="com.ldv.faculty.model.CourseContentItem" %>
<%@ page import="java.util.Collection" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<% Course course = (Course) request.getAttribute("course"); %>
<head>
    <title><%= course.getName() %> Content</title>

    <link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet"/>
</head>
<body>
<h2><%= course.getName() %> Content</h2>
<% if (request.getAttribute("errors") != null) { %>
<ul class="error-block">
    <% for (String error : (Collection<String>) request.getAttribute("errors")) { %>
    <li><%= error %></li>
    <% } %>
</ul>
<% } %>
<form method="post" enctype="multipart/form-data">
    <input type="file" name="contentItem" />
    <button type="submit">Upload</button>
</form>
<% if (course.getContentItems().isEmpty()) { %>
    <p>No content items</p>
<% } else { %>
    <ul>
        <% for (CourseContentItem item : course.getContentItems()) { %>
            <li>
                <a href="content?contentId=<%= item.getId() %>"><%= item.getName() %></a>
            </li>
        <% } %>
    </ul>
<% } %>
<a class="back-btn" href="${pageContext.request.contextPath}/course?id=<%= course.getId() %>">
    <button>Back</button>
</a>
</body>
</html>