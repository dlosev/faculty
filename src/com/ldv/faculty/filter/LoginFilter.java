package com.ldv.faculty.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginFilter implements Filter {

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        HttpSession session = request.getSession(true);

        if (request.getRequestURI().equals(request.getContextPath() + "/logout")) {
            session.removeAttribute("user");

            response.sendRedirect(request.getContextPath() + "/");

            return;
        }

        if (session.getAttribute("user") != null) {
            chain.doFilter(request, response);
        } else {
            response.sendRedirect("login");
        }
    }
}
