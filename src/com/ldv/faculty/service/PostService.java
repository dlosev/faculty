package com.ldv.faculty.service;

import com.ldv.faculty.dao.PostDao;
import com.ldv.faculty.model.Comment;
import com.ldv.faculty.model.Post;

import java.time.LocalDateTime;
import java.util.Collection;

public class PostService {

    private PostDao postDao = new PostDao();

    public void create(Post post) throws Exception {
        post.setCreatedAt(LocalDateTime.now());

        postDao.create(post);
    }

    public void create(Comment comment) throws Exception {
        comment.setCreatedAt(LocalDateTime.now());

        postDao.create(comment);
    }

    public Collection<Post> get(long id) throws Exception {
        return postDao.getByCourse(id);
    }
}
