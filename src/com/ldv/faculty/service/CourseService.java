package com.ldv.faculty.service;

import com.ldv.faculty.dao.CourseDao;
import com.ldv.faculty.model.Course;
import com.ldv.faculty.model.CourseContentItem;
import com.ldv.faculty.model.CourseQuestion;
import com.ldv.faculty.model.Instructor;

import java.util.Collection;
import java.util.List;

public class CourseService {

    private CourseDao courseDao = new CourseDao();

    public Collection<Course> getByInstructor(Instructor instructor) throws Exception {
        return courseDao.getByInstructor(instructor);
    }

    public Course get(long id) throws Exception {
        return courseDao.get(id);
    }

    public List<CourseQuestion> getQuestions(long id) throws Exception {
        return courseDao.getQuestions(id);
    }

    public CourseContentItem getContentItem(long id) throws Exception {
        return courseDao.getContentItem(id);
    }

    public void save(Course course) throws Exception {
        courseDao.save(course);
    }

    public void saveQuestion(CourseQuestion question) throws Exception {
        courseDao.saveQuestion(question);
    }

    public void createContentItem(CourseContentItem contentItem) throws Exception {
        courseDao.createContentItem(contentItem);
    }
}
