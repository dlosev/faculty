package com.ldv.faculty.service;

import com.ldv.faculty.dao.UserDao;
import com.ldv.faculty.model.User;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.security.MessageDigest;

public class UserService {

    private UserDao userDao;

    public UserService() {
        userDao = new UserDao();
    }

    public User getUser(String username, String password) throws Exception {
        User user = userDao.getUser(username);

        return user != null && user.getPassword().equals(getPassword(password)) ? user : null;
    }

    public void register(User user) throws Exception {
        user.setPassword(getPassword(user.getPassword()));

        userDao.create(user);
    }

    private String getPassword(String password) throws Exception {
        return new HexBinaryAdapter().marshal(MessageDigest.getInstance("MD5").digest(password.getBytes()));
    }
}
