package com.ldv.faculty.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionFactory {

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection getConnection() throws Exception {
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/faculty", "faculty", "faculty");
    }
}
