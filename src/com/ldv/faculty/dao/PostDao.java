package com.ldv.faculty.dao;

import com.ldv.faculty.model.Comment;
import com.ldv.faculty.model.Course;
import com.ldv.faculty.model.Post;
import com.ldv.faculty.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PostDao {

    private static final String CREATE_INSERT_QUERY = "INSERT INTO post(text, course_id, user_id, created_at) VALUES(?, ?, ?, ?);";
    private static final String GET_QUERY = "SELECT p.*, u.first_name, u.last_name, u.username FROM post p INNER JOIN course c ON c.id = p.course_id INNER JOIN user u ON u.id = p.user_id WHERE c.id = ? ORDER BY p.id DESC;";
    private static final String COMMENT_INSERT_QUERY = "INSERT INTO comment(text, post_id, user_id, created_at) VALUES(?, ?, ?, ?);";
    private static final String COMMENTS_GET_QUERY = "SELECT c.*, u.first_name, u.last_name, u.username FROM comment c INNER JOIN post p ON p.id = c.post_id INNER JOIN user u ON u.id = c.user_id WHERE p.id IN ";

    public void create(Post post) throws Exception {
        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(CREATE_INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);

            int i = 1;
            ps.setString(i++, post.getText());
            ps.setLong(i++, post.getCourse().getId());
            ps.setLong(i++, post.getUser().getId());
            ps.setTimestamp(i, Timestamp.valueOf(post.getCreatedAt()));

            ps.executeUpdate();

            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                generatedKeys.next();

                post.setId(generatedKeys.getLong(1));
            }
        }
    }

    public void create(Comment comment) throws Exception {
        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(COMMENT_INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);

            int i = 1;
            ps.setString(i++, comment.getText());
            ps.setLong(i++, comment.getPost().getId());
            ps.setLong(i++, comment.getUser().getId());
            ps.setTimestamp(i, Timestamp.valueOf(comment.getCreatedAt()));

            ps.executeUpdate();

            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                generatedKeys.next();

                comment.setId(generatedKeys.getLong(1));
            }
        }
    }

    public Collection<Post> getByCourse(long id) throws Exception {
        List<Post> posts = new ArrayList<>();

        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(GET_QUERY);

            ps.setLong(1, id);
            ps.execute();

            try (ResultSet rs = ps.getResultSet()) {
                while (rs.next()) {
                    posts.add(parsePost(rs));
                }
            }
        }

        loadComments(posts);

        return posts;
    }

    public void loadComments(Collection<Post> posts) throws Exception {
        if (posts.isEmpty()) {
            return;
        }

        String query = "";
        Map<Long, Post> map = new HashMap<>(posts.size());

        for (Post post : posts) {
            map.put(post.getId(), post);
            query += "?,";
        }

        query = COMMENTS_GET_QUERY + "(" + query.substring(0, query.length() - 1) + ") ORDER BY c.id ASC;";

        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(query);

            int i = 1;
            for (Long id : map.keySet()) {
                ps.setLong(i++, id);
            }

            ps.execute();

            try (ResultSet rs = ps.getResultSet()) {
                while (rs.next()) {
                    Comment comment = parseComment(rs);

                    Post post = map.get(comment.getPost().getId());
                    if (post.getComments() == null || post.getComments().isEmpty()) {
                        post.setComments(new ArrayList<>());
                    }

                    post.getComments().add(comment);
                }
            }
        }
    }

    private Post parsePost(ResultSet rs) throws Exception {
        Post post = new Post();

        post.setId(rs.getLong("id"));
        post.setText(rs.getString("text"));
        post.setCreatedAt(rs.getTimestamp("created_at").toLocalDateTime());
        post.setCourse(new Course(rs.getLong("course_id")));
        post.setUser(parseUser(rs));

        return post;
    }

    private Comment parseComment(ResultSet rs) throws Exception {
        Comment comment = new Comment();

        comment.setId(rs.getLong("id"));
        comment.setText(rs.getString("text"));
        comment.setCreatedAt(rs.getTimestamp("created_at").toLocalDateTime());
        comment.setPost(new Post(rs.getLong("post_id")));
        comment.setUser(parseUser(rs));

        return comment;
    }

    private User parseUser(ResultSet rs) throws Exception {
        User user = new User(rs.getLong("user_id"));

        user.setFirstName(rs.getString("first_name"));
        user.setLastName(rs.getString("last_name"));
        user.setUsername(rs.getString("username"));

        return user;
    }
}
