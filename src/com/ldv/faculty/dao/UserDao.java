package com.ldv.faculty.dao;

import com.ldv.faculty.model.Instructor;
import com.ldv.faculty.model.Student;
import com.ldv.faculty.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class UserDao {

    private static final String USER_QUERY = "SELECT u.*, s.id AS student_id, i.id AS instructor_id FROM user u LEFT JOIN student s ON s.id = u.id LEFT JOIN instructor i ON i.id = u.id WHERE u.username = ?;";

    private static final String USER_INSERT_QUERY = "INSERT INTO user(first_name, last_name, username, password) VALUES (?, ?, ?, ?);";
    private static final String STUDENT_INSERT_QUERY = "INSERT INTO student(id) VALUES (?);";
    private static final String INSTRUCTOR_INSERT_QUERY = "INSERT INTO instructor(id) VALUES (?);";

    public User getUser(String username) throws Exception {
        User user = null;

        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(USER_QUERY);

            ps.setString(1, username);
            ps.execute();

            try (ResultSet rs = ps.getResultSet()) {
                if (rs.next()) {
                    user = parseUser(rs);
                }
            }
        }

        return user;
    }

    public void create(User user) throws Exception {
        User u = getUser(user.getUsername());

        if (u != null) {
            throw new RuntimeException("User with such ID already exists");
        }

        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(USER_INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);

            int i = 1;
            ps.setString(i++, user.getFirstName());
            ps.setString(i++, user.getLastName());
            ps.setString(i++, user.getUsername());
            ps.setString(i, user.getPassword());

            ps.executeUpdate();

            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                generatedKeys.next();

                user.setId(generatedKeys.getLong(1));
            }

            ps = connection.prepareStatement(user instanceof Instructor ? INSTRUCTOR_INSERT_QUERY : STUDENT_INSERT_QUERY);

            ps.setLong(1, user.getId());

            ps.executeUpdate();
        }
    }

    private User parseUser(ResultSet rs) throws Exception {
        rs.getLong("student_id");

        User user = rs.wasNull() ? new Instructor() : new Student();

        user.setId(rs.getLong("id"));
        user.setFirstName(rs.getString("first_name"));
        user.setLastName(rs.getString("last_name"));
        user.setUsername(rs.getString("username"));
        user.setPassword(rs.getString("password"));

        return user;
    }
}
