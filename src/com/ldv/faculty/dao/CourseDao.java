package com.ldv.faculty.dao;

import com.ldv.faculty.model.Course;
import com.ldv.faculty.model.CourseContentItem;
import com.ldv.faculty.model.CourseQuestion;
import com.ldv.faculty.model.Instructor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CourseDao {

    private static final String GET_QUERY = "SELECT c.* FROM course c WHERE c.id = ?;";
    private static final String CONTENT_ITEM_GET_QUERY = "SELECT * FROM course_content_item WHERE id = ?;";
    private static final String CONTENT_ITEMS_GET_QUERY = "SELECT cci.id, cci.name, cci.created_at FROM course_content_item cci INNER JOIN course c ON c.id = cci.course_id WHERE c.id = ?;";
    private static final String QUESTIONS_GET_QUERY = "SELECT cq.* FROM course_question cq INNER JOIN course c ON c.id = cq.course_id WHERE c.id = ?;";
    private static final String GET_BY_INSTRUCTOR_QUERY = "SELECT c.* FROM course c INNER JOIN instructor i ON i.id = c.instructor_id WHERE i.id = ?;";
    private static final String COURSE_INSERT_QUERY = "INSERT INTO course(name, description, instructor_id) VALUES (?, ?, ?);";
    private static final String COURSE_UPDATE_QUERY = "UPDATE course SET name = ?, description = ? WHERE id = ?;";
    private static final String CONTENT_ITEM_INSERT_QUERY = "INSERT INTO course_content_item(name, data, created_at, course_id) VALUES (?, ?, ?, ?);";
    private static final String QUESTION_INSERT_QUERY = "INSERT INTO course_question(text, answer1, answer2, answer3, answer4, correct_answer_number, course_id) VALUES (?, ?, ?, ?, ?, ?, ?);";

    public Course get(long id) throws Exception {
        Course course = null;

        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(GET_QUERY);

            ps.setLong(1, id);
            ps.execute();

            try (ResultSet rs = ps.getResultSet()) {
                if (rs.next()) {
                    course = parseCourse(rs);

                    course.setContentItems(getContentItems(course.getId()));
                }
            }
        }

        return course;
    }

    public List<CourseQuestion> getQuestions(long id) throws Exception {
        List<CourseQuestion> questions = new ArrayList<>();

        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(QUESTIONS_GET_QUERY);

            ps.setLong(1, id);
            ps.execute();

            try (ResultSet rs = ps.getResultSet()) {
                while (rs.next()) {
                    questions.add(parseCourseQuestion(rs));
                }
            }
        }

        return questions;
    }

    public CourseContentItem getContentItem(long id) throws Exception {
        CourseContentItem contentItem = null;

        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(CONTENT_ITEM_GET_QUERY);

            ps.setLong(1, id);
            ps.execute();

            try (ResultSet rs = ps.getResultSet()) {
                if (rs.next()) {
                    contentItem = parseContentItem(rs);

                    contentItem.setData(rs.getBytes("data"));
                }
            }
        }

        return contentItem;
    }

    public Collection<CourseContentItem> getContentItems(long courseId) throws Exception {
        Collection<CourseContentItem> result = new ArrayList<>();

        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(CONTENT_ITEMS_GET_QUERY);

            ps.setLong(1, courseId);
            ps.execute();

            try (ResultSet rs = ps.getResultSet()) {
                while (rs.next()) {
                    result.add(parseContentItem(rs));
                }
            }
        }

        return result;
    }

    public Collection<Course> getByInstructor(Instructor instructor) throws Exception {
        Collection<Course> result = new ArrayList<>();

        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(GET_BY_INSTRUCTOR_QUERY);

            ps.setLong(1, instructor.getId());
            ps.execute();

            try (ResultSet rs = ps.getResultSet()) {
                while (rs.next()) {
                    result.add(parseCourse(rs));
                }
            }
        }

        return result;
    }

    public void saveQuestion(CourseQuestion question) throws Exception {
        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(QUESTION_INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);

            int i = 1;
            ps.setString(i++, question.getText());
            ps.setString(i++, question.getAnswer1());
            ps.setString(i++, question.getAnswer2());
            ps.setString(i++, question.getAnswer3());
            ps.setString(i++, question.getAnswer4());
            ps.setInt(i++, question.getCorrectAnswerNumber());
            ps.setLong(i, question.getCourse().getId());

            ps.executeUpdate();

            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    question.setId(generatedKeys.getLong(1));
                }
            }
        }
    }

    public void createContentItem(CourseContentItem contentItem) throws Exception {
        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(CONTENT_ITEM_INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);

            int i = 1;
            ps.setString(i++, contentItem.getName());
            ps.setBytes(i++, contentItem.getData());
            ps.setTimestamp(i++, Timestamp.valueOf(contentItem.getCreatedAt()));
            ps.setLong(i, contentItem.getCourse().getId());

            ps.executeUpdate();

            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    contentItem.setId(generatedKeys.getLong(1));
                }
            }
        }
    }

    public void save(Course course) throws Exception {
        try (Connection connection = ConnectionFactory.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(course.getId() == null ?
                    COURSE_INSERT_QUERY : COURSE_UPDATE_QUERY, Statement.RETURN_GENERATED_KEYS);

            int i = 1;
            ps.setString(i++, course.getName());
            ps.setString(i++, course.getDescription());

            ps.setLong(i, course.getId() == null ? course.getInstructor().getId() : course.getId());

            ps.executeUpdate();

            if (course.getId() == null) {
                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        course.setId(generatedKeys.getLong(1));
                    }
                }
            }
        }
    }

    private Course parseCourse(ResultSet rs) throws Exception {
        Course course = new Course();

        course.setId(rs.getLong("id"));
        course.setName(rs.getString("name"));
        course.setDescription(rs.getString("description"));
        course.setInstructor(new Instructor(rs.getLong("instructor_id")));

        return course;
    }

    private CourseContentItem parseContentItem(ResultSet rs) throws Exception {
        CourseContentItem contentItem = new CourseContentItem();

        contentItem.setId(rs.getLong("id"));
        contentItem.setName(rs.getString("name"));
        contentItem.setCreatedAt(rs.getTimestamp("created_at").toLocalDateTime());

        return contentItem;
    }

    private CourseQuestion parseCourseQuestion(ResultSet rs) throws Exception {
        CourseQuestion courseQuestion = new CourseQuestion();

        courseQuestion.setId(rs.getLong("id"));
        courseQuestion.setText(rs.getString("text"));
        courseQuestion.setAnswer1(rs.getString("answer1"));
        courseQuestion.setAnswer2(rs.getString("answer2"));
        courseQuestion.setAnswer3(rs.getString("answer3"));
        courseQuestion.setAnswer4(rs.getString("answer4"));
        courseQuestion.setCorrectAnswerNumber(rs.getInt("correct_answer_number"));

        return courseQuestion;
    }
}
