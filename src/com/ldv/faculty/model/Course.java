package com.ldv.faculty.model;

import java.util.Collection;

public class Course {

    private Long id;

    private String name;

    private String description;

    private Instructor instructor;

    private Collection<CourseContentItem> contentItems;

    private Collection<CourseQuestion> questions;

    public Course() {
    }

    public Course(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }



    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public Collection<CourseContentItem> getContentItems() {
        return contentItems;
    }

    public void setContentItems(Collection<CourseContentItem> contentItems) {
        this.contentItems = contentItems;
    }

    public Collection<CourseQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(Collection<CourseQuestion> questions) {
        this.questions = questions;
    }
}
