package com.ldv.faculty.servlet;

import com.ldv.faculty.model.User;
import com.ldv.faculty.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    private UserService userService;

    @Override
    public void init() throws ServletException {
        super.init();

        userService = new UserService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user;

        try {
            user = userService.getUser(request.getParameter("username"), request.getParameter("password"));
        } catch (Exception e) {
            throw new ServletException(e);
        }

        if (user == null) {
            request.setAttribute("error", "Wrong username or password");

            getServletContext().getRequestDispatcher("/login.jsp").forward(request,response);

            return;
        }

        request.getSession(true).setAttribute("user", user);

        response.sendRedirect(request.getContextPath() + "/");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession(true).getAttribute("user") == null) {
            getServletContext().getRequestDispatcher("/login.jsp").forward(request,response);
        } else {
            response.sendRedirect(request.getContextPath() + "/");
        }
    }
}
