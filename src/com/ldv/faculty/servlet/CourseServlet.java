package com.ldv.faculty.servlet;

import com.ldv.faculty.model.Comment;
import com.ldv.faculty.model.Course;
import com.ldv.faculty.model.CourseContentItem;
import com.ldv.faculty.model.CourseQuestion;
import com.ldv.faculty.model.Instructor;
import com.ldv.faculty.model.Post;
import com.ldv.faculty.model.User;
import com.ldv.faculty.service.CourseService;
import com.ldv.faculty.service.PostService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@MultipartConfig
public class CourseServlet extends HttpServlet {

    private CourseService courseService = new CourseService();

    private PostService postService = new PostService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if ((request.getContextPath() + "/course/new").equals(request.getRequestURI())) {
            Course course = new Course();

            course.setName(request.getParameter("name"));
            course.setDescription(request.getParameter("description"));
            course.setInstructor((Instructor) request.getSession().getAttribute("user"));

            request.setAttribute("course", course);

            Collection<String> error = validate(course);

            if (!error.isEmpty()) {
                request.setAttribute("errors", error);

                getServletContext().getRequestDispatcher("/course-new.jsp").forward(request, response);

                return;
            }

            try {
                courseService.save(course);
            } catch (Exception e) {
                throw new ServletException(e);
            }

            response.sendRedirect(request.getContextPath() + "/course");
        } else if ((request.getContextPath() + "/course/content").equals(request.getRequestURI())) {
            long id = Long.valueOf(request.getParameter("id"));

            Course course;

            try {
                course = courseService.get(id);
            } catch (Exception e) {
                throw new ServletException(e);
            }

            request.setAttribute("course", course);

            Part file = request.getPart("contentItem");
            if (file.getSubmittedFileName() != null && file.getSubmittedFileName().length() > 0) {
                if (file.getSize() > 1024 * 1024 * 10) {
                    request.setAttribute("errors", Collections.singletonList("Maximum uploading file size is 10 MB"));

                    getServletContext().getRequestDispatcher("/course-content.jsp").forward(request, response);

                    return;
                }

                saveContentItem(file, course);

                response.sendRedirect(request.getContextPath() + "/course/content?id=" + id);
            } else {
                request.setAttribute("errors", Collections.singletonList("Please select file to upload"));

                getServletContext().getRequestDispatcher("/course-content.jsp").forward(request, response);
            }
        } else if ((request.getContextPath() + "/course/questions").equals(request.getRequestURI())) {
            Collection<CourseQuestion> questions = new ArrayList<>(2);

            Course course = new Course(Long.valueOf(request.getParameter("id")));
            CourseQuestion question1 = new CourseQuestion();

            question1.setText(request.getParameter("text1"));
            question1.setAnswer1(request.getParameter("answer11"));
            question1.setAnswer2(request.getParameter("answer12"));
            question1.setAnswer3(request.getParameter("answer13"));
            question1.setAnswer4(request.getParameter("answer14"));
            question1.setCorrectAnswerNumber(Integer.valueOf(request.getParameter("correctAnswerNumber1")));
            question1.setCourse(course);

            questions.add(question1);

            CourseQuestion question2 = new CourseQuestion();

            question2.setText(request.getParameter("text2"));
            question2.setAnswer1(request.getParameter("answer21"));
            question2.setAnswer2(request.getParameter("answer22"));
            question2.setAnswer3(request.getParameter("answer23"));
            question2.setAnswer4(request.getParameter("answer24"));
            question2.setCorrectAnswerNumber(Integer.valueOf(request.getParameter("correctAnswerNumber2")));
            question2.setCourse(course);

            questions.add(question2);

            request.setAttribute("questions", questions);

            Collection<String> errors = validate(question1);
            if (errors.isEmpty()) {
                errors = validate(question2);
            }

            if (!errors.isEmpty()) {
                request.setAttribute("errors", errors);

                getServletContext().getRequestDispatcher("/questions.jsp").forward(request, response);

                return;
            }

            try {
                courseService.saveQuestion(question1);
                courseService.saveQuestion(question2);
            } catch (Exception e) {
                throw new ServletException(e);
            }

            response.sendRedirect(request.getContextPath() + "/course/assignment?id=" + request.getParameter("id"));
        } else if ((request.getContextPath() + "/course/discussion-board").equals(request.getRequestURI())) {
            long id = Long.valueOf(request.getParameter("id"));

            Post post = new Post();

            post.setText(request.getParameter("message"));
            post.setCourse(new Course(id));
            post.setUser((User) request.getSession().getAttribute("user"));

            request.setAttribute("post", post);

            Comment comment = new Comment();

            comment.setText(request.getParameter("comment"));
            comment.setUser((User) request.getSession().getAttribute("user"));

            request.setAttribute("comment", comment);

            try {
                request.setAttribute("posts", postService.get(id));
            } catch (Exception e) {
                throw new ServletException(e);
            }

            if (request.getParameter("postId") != null && request.getParameter("postId").length() > 0) {
                long postId = Long.valueOf(request.getParameter("postId"));

                comment.setPost(new Post(postId));

                Collection<String> error = validate(comment);

                if (!error.isEmpty()) {
                    request.setAttribute("errors", error);

                    getServletContext().getRequestDispatcher("/discussion-board.jsp").forward(request, response);

                    return;
                }

                try {
                    postService.create(comment);
                } catch (Exception e) {
                    throw new ServletException(e);
                }
            } else {
                Collection<String> error = validate(post);

                if (!error.isEmpty()) {
                    request.setAttribute("errors", error);

                    getServletContext().getRequestDispatcher("/discussion-board.jsp").forward(request, response);

                    return;
                }

                try {
                    postService.create(post);
                } catch (Exception e) {
                    throw new ServletException(e);
                }
            }

            response.sendRedirect(request.getContextPath() + "/course/discussion-board?id=" + id);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if ((request.getContextPath() + "/course/new").equals(request.getRequestURI())) {
            request.setAttribute("course", new Course());

            getServletContext().getRequestDispatcher("/course-new.jsp").forward(request, response);
        } else if ((request.getContextPath() + "/course").equals(request.getRequestURI())) {
            if (request.getParameter("id") != null) {
                long id = Long.valueOf(request.getParameter("id"));

                try {
                    request.setAttribute("course", courseService.get(id));
                } catch (Exception e) {
                    throw new ServletException(e);
                }

                getServletContext().getRequestDispatcher("/course.jsp").forward(request, response);
            } else {
                Instructor instructor = (Instructor) request.getSession().getAttribute("user");

                try {
                    request.setAttribute("courses", courseService.getByInstructor(instructor));
                } catch (Exception e) {
                    throw new ServletException(e);
                }

                request.getRequestDispatcher("courses-list.jsp").forward(request, response);
            }
        } else if ((request.getContextPath() + "/course/content").equals(request.getRequestURI())) {
            if (request.getParameter("id") != null && request.getParameter("id").length() != 0) {
                long id = Long.valueOf(request.getParameter("id"));

                try {
                    request.setAttribute("course", courseService.get(id));
                } catch (Exception e) {
                    throw new ServletException(e);
                }

                request.getRequestDispatcher("/course-content.jsp").forward(request, response);
            } else if (request.getParameter("contentId") != null && request.getParameter("contentId").length() != 0) {
                long contentId = Long.valueOf(request.getParameter("contentId"));

                try {
                    CourseContentItem contentItem = courseService.getContentItem(contentId);

                    response.setContentType("application/octet-stream");
                    response.setHeader("Content-Disposition", String.format("attachment; filename=%s", contentItem.getName()));
                    response.setStatus(HttpServletResponse.SC_OK);

                    response.getOutputStream().write(contentItem.getData());

                    response.getOutputStream().flush();
                    response.getOutputStream().close();
                } catch (Exception e) {
                    throw new ServletException(e);
                }
            }
        } else if ((request.getContextPath() + "/course/assignment").equals(request.getRequestURI())) {
            long id = Long.valueOf(request.getParameter("id"));

            try {
                request.setAttribute("questions", courseService.getQuestions(id));
            } catch (Exception e) {
                throw new ServletException(e);
            }

            request.getRequestDispatcher("/assignment.jsp").forward(request, response);
        } else if ((request.getContextPath() + "/course/questions").equals(request.getRequestURI())) {
            long id = Long.valueOf(request.getParameter("id"));

            Collection<CourseQuestion> questions;

            try {
                questions = courseService.getQuestions(id);
            } catch (Exception e) {
                throw new ServletException(e);
            }

            if (questions.isEmpty()) {
                questions.add(new CourseQuestion());
                questions.add(new CourseQuestion());

                request.setAttribute("questions", questions);

                request.getRequestDispatcher("/questions.jsp").forward(request, response);
            } else {
                response.sendRedirect(request.getContextPath() + "/course/assignment?id=" + id);
            }
        } else if ((request.getContextPath() + "/course/discussion-board").equals(request.getRequestURI())) {
            long id = Long.valueOf(request.getParameter("id"));

            request.setAttribute("post", new Post());
            request.setAttribute("comment", new Comment());

            try {
                request.setAttribute("posts", postService.get(id));
            } catch (Exception e) {
                throw new ServletException(e);
            }

            request.getRequestDispatcher("/discussion-board.jsp").forward(request, response);
        }
    }

    private void saveContentItem(Part file, Course course) throws ServletException {
        CourseContentItem contentItem = new CourseContentItem();

        contentItem.setCourse(course);
        contentItem.setName(file.getSubmittedFileName());
        contentItem.setCreatedAt(LocalDateTime.now());

        try {
            try (InputStream is = file.getInputStream()) {
                contentItem.setData(readFile(is));
            }

            courseService.createContentItem(contentItem);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    private Collection<String> validate(Post post) {
        Collection<String> errors = new ArrayList<>();

        if (post.getText() == null || post.getText().length() == 0) {
            errors.add("Message is required");
        }

        return errors;
    }

    private Collection<String> validate(Comment comment) {
        Collection<String> errors = new ArrayList<>();

        if (comment.getText() == null || comment.getText().length() == 0) {
            errors.add("Message is required");
        }

        return errors;
    }

    private Collection<String> validate(Course course) {
        Collection<String> errors = new ArrayList<>();

        if (course.getName() == null || course.getName().length() == 0) {
            errors.add("Name is required");
        }
        if (course.getDescription() == null || course.getDescription().length() == 0) {
            errors.add("Info is required");
        }

        return errors;
    }

    private Collection<String> validate(CourseQuestion question) {
        Collection<String> errors = new ArrayList<>();

        if (question.getText() == null || question.getText().length() == 0) {
            errors.add("Question is required");
        }
        if (question.getAnswer1() == null || question.getAnswer1().length() == 0) {
            errors.add("Option 1 is required");
        }
        if (question.getAnswer2() == null || question.getAnswer2().length() == 0) {
            errors.add("Option 2 is required");
        }
        if (question.getAnswer3() == null || question.getAnswer3().length() == 0) {
            errors.add("Option 3 is required");
        }
        if (question.getAnswer4() == null || question.getAnswer4().length() == 0) {
            errors.add("Option 4 is required");
        }

        return errors;
    }

    public byte[] readFile(InputStream is) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[1024];

        while ((nRead = is.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        buffer.flush();

        return buffer.toByteArray();
    }
}
