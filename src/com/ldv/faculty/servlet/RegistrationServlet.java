package com.ldv.faculty.servlet;

import com.ldv.faculty.model.Instructor;
import com.ldv.faculty.model.Student;
import com.ldv.faculty.model.User;
import com.ldv.faculty.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class RegistrationServlet extends HttpServlet {

    private UserService userService;

    @Override
    public void init() throws ServletException {
        super.init();

        userService = new UserService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = Boolean.valueOf(request.getParameter("instructor")) ? new Instructor() : new Student();

        user.setUsername(request.getParameter("username"));
        user.setPassword(request.getParameter("password"));
        user.setPasswordConfirm(request.getParameter("passwordConfirm"));
        user.setFirstName(request.getParameter("firstName"));
        user.setLastName(request.getParameter("lastName"));

        request.setAttribute("user", user);

        Collection<String> error = validate(user);

        if (!error.isEmpty()) {
            request.setAttribute("errors", error);

            getServletContext().getRequestDispatcher("/registration.jsp").forward(request, response);

            return;
        }

        try {
            userService.register(user);
        } catch (Exception e) {
            request.setAttribute("errors", Collections.singletonList(e.getMessage()));

            getServletContext().getRequestDispatcher("/registration.jsp").forward(request, response);

            return;
        }

        request.getSession(true).setAttribute("user", user);

        response.sendRedirect(request.getContextPath() + "/");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession(true).getAttribute("user") == null) {
            request.setAttribute("user", new User());

            getServletContext().getRequestDispatcher("/registration.jsp").forward(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + "/");
        }
    }

    private Collection<String> validate(User user) {
        Collection<String> errors = new ArrayList<>();

        if (user.getFirstName() == null || user.getFirstName().length() == 0) {
            errors.add("First name is required");
        }
        if (user.getLastName() == null || user.getLastName().length() == 0) {
            errors.add("Last name is required");
        }
        if (user.getUsername() == null || user.getUsername().length() == 0) {
            errors.add("ID is required");
        }
        if (user.getPassword() == null || user.getPassword().length() == 0) {
            errors.add("Password is required");
        } else if (!user.getPassword().equals(user.getPasswordConfirm())) {
            errors.add("Password confirmation and password are different");
        }

        return errors;
    }
}
