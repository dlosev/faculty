CREATE DATABASE faculty;
CREATE USER 'faculty'@'localhost' IDENTIFIED BY 'faculty';
GRANT ALL PRIVILEGES ON faculty.* TO 'faculty'@'localhost';
FLUSH PRIVILEGES;

CREATE TABLE user (
  id INTEGER NOT NULL AUTO_INCREMENT,
  first_name CHAR(32) NOT NULL,
  last_name CHAR(32) NOT NULL,
  username CHAR(32) NOT NULL,
  password CHAR(32) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY(username)
);

CREATE TABLE student (
  id INTEGER NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY `fk_student_user` (id) REFERENCES user(id) ON DELETE CASCADE
);

CREATE TABLE instructor (
  id INTEGER NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY `fk_instructor_user` (id) REFERENCES user(id) ON DELETE CASCADE
);

CREATE TABLE course (
  id INTEGER NOT NULL AUTO_INCREMENT,
  name CHAR(255) NOT NULL,
  description TEXT NOT NULL,
  instructor_id INTEGER NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY `fk_course_instructor` (instructor_id) REFERENCES instructor(id) ON DELETE CASCADE
);

CREATE TABLE course_content_item (
  id INTEGER NOT NULL AUTO_INCREMENT,
  name CHAR(255) NOT NULL,
  data MEDIUMBLOB NOT NULL,
  created_at DATETIME NOT NULL,
  course_id INTEGER NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY `fk_course_content_item_course` (course_id) REFERENCES course(id) ON DELETE CASCADE
);

CREATE TABLE course_question (
  id INTEGER NOT NULL AUTO_INCREMENT,
  text TEXT NOT NULL,
  answer1 TEXT NOT NULL,
  answer2 TEXT NOT NULL,
  answer3 TEXT NOT NULL,
  answer4 TEXT NOT NULL,
  correct_answer_number INT NOT NULL,
  course_id INTEGER NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY `fk_course_question_course` (course_id) REFERENCES course(id) ON DELETE CASCADE
);

CREATE TABLE post (
  id INTEGER NOT NULL AUTO_INCREMENT,
  text TEXT NOT NULL,
  course_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  created_at DATETIME NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY `fk_post_course` (course_id) REFERENCES course(id) ON DELETE CASCADE,
  FOREIGN KEY `fk_post_user` (user_id) REFERENCES user(id) ON DELETE CASCADE
);

CREATE TABLE comment (
  id INTEGER NOT NULL AUTO_INCREMENT,
  text TEXT NOT NULL,
  post_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  created_at DATETIME NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY `fk_comment_post` (post_id) REFERENCES post(id) ON DELETE CASCADE,
  FOREIGN KEY `fk_post_user` (user_id) REFERENCES user(id) ON DELETE CASCADE
);